from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from post.forms import PostForm
from post.models import Post


def index(request):
    posts = Post.objects.all()
    form = PostForm()
    return render(
        request, 'index.html',
        context={'posts': posts, 'form': form})


@login_required
def add_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            Post.objects.create(
                text=form.cleaned_data['text'],
                author=request.user)
            return HttpResponseRedirect('/')

