from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin, StackedInline

from post.models import Post


class PostInline(StackedInline):
    model = Post
    extra = 1


@admin.register(Post)
class PostAdmin(ModelAdmin):
    list_display = [
        'text',
        'author',
    ]

    search_fields = [
        'text',
        'author__username',
    ]

    list_filter = [
        'author__username'
    ]

    autocomplete_fields = [
        'author'
    ]