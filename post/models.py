from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import CASCADE


class Post(models.Model):
    text = models.TextField()
    author = models.ForeignKey(get_user_model(), on_delete=CASCADE)

    class Meta:
        db_table = 'post'
        verbose_name = 'пост'
        verbose_name_plural = 'посты'
