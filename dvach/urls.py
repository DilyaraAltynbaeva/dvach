from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include

from post.views import index, add_post

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('add/', add_post, name='add'),
    # path('login/', LoginView.as_view(template_name='user/login.html'), name='login'),
    # path('logout/', LogoutView.as_view(), name='logout'),
    path('auth/', include('user.urls')),
]
