from django.contrib import admin
from django.contrib.admin import ModelAdmin

from post.admin import PostInline
from post.models import Post
from user.models import User


@admin.register(User)
class UserAdmin(ModelAdmin):
    search_fields = [
        'username',
    ]
    list_display = [
        'username',
        'post_ids_display',
        'post_exists_display',
        'is_active',
    ]

    date_hierarchy = 'date_joined'

    def post_ids_display(self, obj):
        return ', '.join(
            [str(post.id) for post in Post.objects.filter(author=obj)])

    post_ids_display.short_description = 'ID постов'

    def post_exists_display(self, obj):
        return Post.objects.filter(author=obj).exists()

    post_exists_display.short_description = 'Есть посты'
    post_exists_display.boolean = True

    inlines = [PostInline]

    actions = ['add_post']

    def add_post(self, request, queryset):
        for u in queryset:
            Post.objects.create(author=u, text='hello')

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False